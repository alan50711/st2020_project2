const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(4) > button', (content) => content.innerHTML);
    expect(title).toBe('Join');
    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: 'test/screenshots/login.png'});
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'room1', 100);
    await page.click('body > div > form > div:nth-child(4) > button',100);
    await page.screenshot({path: 'test/screenshots/welcome.png'});
    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.click('body > div > form > div:nth-child(4) > button');
    await page.screenshot({path: 'test/screenshots/error.png'});
    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'room2', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('room2');
    await page.click('body > div > form > div:nth-child(4) > button',100);
    const divsCounts = await page.$$eval('#messages', divs => divs.length);
    // console.log(divsCounts)
    expect(divsCounts).not.toBe(0);
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'room3', 100);
    let roomName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(roomName).toBe('room3');

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})

// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page1 = await browser.newPage();
    const page2 = await browser.newPage();

    await page1.goto(url);
    await page2.goto(url);

    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 

    await page1.waitForSelector('#users > ol > li');    
    let member1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    expect(member1).toBe('John');

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Tony', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R2', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 

    await page2.waitForSelector('#users > ol > li');    
    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member2).toBe('Tony');
    
    await browser.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R3', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    let sendButton = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });
    expect(sendButton).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R4', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
   
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    expect(member).toBe('John');



    let sendButton = await page.evaluate(() => {
        return document.querySelector('#message-form > button').innerHTML;
    });
    expect(sendButton).toBe('Send');

    await page.type('#message-form > input[type=text]', 'test message', {delay: 100});
    await page.keyboard.press('Enter', {delay: 1000}); 


    await browser.close();
})

// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser1 = await puppeteer.launch({ headless: !openBrowser });
    const browser2 = await puppeteer.launch({ headless: !openBrowser });

    const page1 = await browser1.newPage();
    const page2 = await browser2.newPage();

    await page1.goto(url);
    await page2.goto(url);

    await page1.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page1.type('body > div > form > div:nth-child(3) > input[type=text]', 'R5', {delay: 100});
    await page1.keyboard.press('Enter', {delay: 100}); 


    await page1.waitForSelector('#users > ol > li');    
    let member1 = await page1.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    expect(member1).toBe('John');

    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R5', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 


    await page2.waitForSelector('#users > ol > li');    
    let member2 = await page2.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(2)').innerHTML;
    });

    expect(member2).toBe('Mike');
    

    await page1.type('#message-form > input[type=text]', 'Hi');
    await page1.keyboard.press('Enter', {delay: 100}); 

    await page2.type('#message-form > input[type=text]', 'Hello');
    await page2.keyboard.press('Enter', {delay: 100}); 

    await browser1.close();
    await browser2.close();

})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R6', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
   
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    expect(member).toBe('John');



    let sendLocationButton = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(sendLocationButton).toBe('Send location');

    await browser.close();
})

// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R7', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 
   
    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li:nth-child(1)').innerHTML;
    });

    expect(member).toBe('John');

    let sendLocationButton = await page.evaluate(() => {
        return document.querySelector('#send-location').innerHTML;
    });
    expect(sendLocationButton).toBe('Send location');
    await page.setGeolocation({
        latitude: 51.5074,
        longitude: 0.1278
      });

    const context = browser.defaultBrowserContext();
    await context.overridePermissions(url, ['geolocation']);

    
    await page.waitForSelector('#send-location');
    await page.$eval('#send-location', elem => elem.click());
    
    await browser.close();

})
